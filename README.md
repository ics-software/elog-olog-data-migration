# Olog - migration service

This is a modified version of the Olog-es service that allows setting the created date and author. It comes together with a set of Python scripts to export data from existing ELOG logbooks into Olog.

The migration is done in steps, and each step is documented here:
- [First migration - 2021 Sept. 21](python/1_Migration_Operation_OnCall_Maintenance_Studies/README.md)
- [Second migration - 2022 April 13](python/2_Migration_TS2/README.md)
- [Third migration - 2022 April 26](python/3_Migration_PBI_VAC/README.md)
- [Fourth migration - 2022 May 24](python/4_Migration_Cryo_RFS/README.md)
