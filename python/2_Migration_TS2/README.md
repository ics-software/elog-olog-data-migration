# Second migration - 2022 April 13

During this migration, the TS2 logbook was migrated from ELOG to Olog. Below it is described the process followed. The Python notebooks contain the output generated when they were run.

The server has already logbooks, tags, and properties created by Georg.

## Migration procedure
1. INFRA stops Olog server and starts the migration service (https://gitlab.esss.lu.se/ics-software/elog-olog-data-migration).

2. Backup logbook ELOG production server
	1. `ssh logbook`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `tar cvfz TS2.tar.gz TS2`
	5. `chown juanfestebanmuller:elog TS2.tar.gz`

3. Copy entries to ELOG test server
	1. `ssh ics-elog-test`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `rm -rf TS2`
	5. `scp juanfestebanmuller@logbook.esss.lu.se:/usr/local/elog/logbooks/TS2.tar.gz .`
	6. `tar xvfz TS2.tar.gz`
	7. `systemctl restart elogd`

4. Add all attributes in elog test server, so that when exporting entries they contain all possible metadata:
	
       Attributes = Author, Shift ID, Entry Type, Insert Template, Tags, Subject, Discipline, Subsection, Device Group, Section, Category, Entry Class

5. Import TS2 logbook using the Python notebook `olog_mig_TS2.ipynb`.

6. Then we closed TS2 logbook in ELOG production server. To do that, the following lines are added to the configuration file:
   1. Add comment about closure: 
 
          Comment = Logbook for test stand 2 <br><h1 style="color:tomato">NOTE: THIS LOGBOOK IS CLOSED FOR NEW ENTRIES. PLEASE USE OLOG INSTEAD: <a href="https://olog-ts2.esss.lu.se">https://olog-ts2.esss.lu.se</a></h1><br>

	2. Remove options to create new entries, reply, or duplicate.

            List Menu commands = Find, Config, Admin, Logout, Help
            Menu commands = List, Help

7. Migrating 2 entries that were created during the execution of steps 2-5.
	1. Repeat steps 2 and 3.
	2. Import using olog_mig_TS2_new_entries.ipynb

8. Finally INFRA stops the migration service and starts Olog.