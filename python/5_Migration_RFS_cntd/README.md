# Fifth migration - 2022 November 2

During this migration, the latest entries from the RFS logbooks were migrated from ELOG to Olog. This comprises entries posted after May 24, 2022.
Below it is described the process followed. The Python notebooks contain the output generated when they were run.

## Tests
Tests were carried out on Oct. 4, 2022.

First attempt was done on Nov. 1, but an Olog update since the tests were run broke the migration service. The Docker image of the migration service was updated and migration took place the next day.

## Migration procedure

The migration into the production server took place on Nov. 2, 2022, at around 16:30.

1. First we closed all logbooks in ELOG production server. To do that, the following lines of the configuration file are modified:
   1. Add comment about closure in the global section of the configuration file: 

          Welcome Title = <p><font size=4 color=white>This logbook has been archived and all data migrated to the new logbooks:<p>Operations logbook:<a href=https://olog.esss.lu.se>https://olog.esss.lu.se</a><p>Test Stand logbook:<a href=https://olog-ts2.esss.lu.se>https://olog-ts2.esss.lu.se</a></font><p><font size=3 color=red>It is not possible to create new entries in this logbook.</font>

	2. Remove options to create new entries, reply, or duplicate.

            List Menu commands = Find, Config, Admin, Logout, Help
            Menu commands = List, Help

	3. Restore the 'Comment' option for the logbooks that were closed during the previous migrations: `Cryo`, `PBI`, `RFS`, `Vac`, `Maintenance`, `Operations`, `Studies`, `On-call`, and `TS2`.

	These steps are done by copying the config file available in the same directory as this README.md.

2. Backup logbook ELOG production server (only RFS logbook for 2022)
	1. `ssh logbook`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/RFS`
	4. `tar cvfz RFS_2022.tar.gz 2022`
	5. `chown juanfestebanmuller:elog RFS_2022.tar.gz`

3. Copy entries to ELOG test server
	1. `ssh ics-elog-test`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/RFS`
	4. `rm -rf 2022`
	5. `scp juanfestebanmuller@logbook.esss.lu.se:/usr/local/elog/logbooks/RFS/RFS_2022.tar.gz .`
	6. `tar xvfz RFS_2022.tar.gz`
	7. `systemctl restart elogd`

4. Check that all attributes are listed in elog test server (done during previous migration), so that when exporting entries they contain all possible metadata:
	
       Attributes = Author, Shift ID, Entry Type, Insert Template, Tags, Subject, Discipline, Subsection, Device Group, Section, Category, Entry Class

5. INFRA starts Olog migration serverice (https://gitlab.esss.lu.se/ics-software/ess-olog-product/-/tree/elog-olog-data-migration , https://gitlab.esss.lu.se/ics-software/elog-olog-data-migration).

6. Import entries with ID>450 from RFS logbook using the Python notebook `olog_mig_RFS.ipynb`.

7. Finally INFRA stops the migration service.