# Third migration - 2022 April 26

During this migration, the Vac and PBI logbooks were migrated from ELOG to Olog. Below it is described the process followed. The Python notebooks contain the output generated when they were run.

Both logbooks are merged into the Operations logbook in the operations instance (olog.esss.lu.se). Therefore, the server has already logbooks, tags, and properties created.

## Migration procedure

The first 4 steps were done on April 22, 2022. The migration into the production server took place on April 26, 2022, at around 10:10.

1. First we closed PBI and Vac logbooks in ELOG production server. To do that, the following lines are added to the configuration file:
   1. Add comment about closure: 

          Comment = Vacuum System<br><h1 style="color:tomato">NOTE: THIS LOGBOOK IS CLOSED FOR NEW ENTRIES. PLEASE USE OLOG INSTEAD: <a href="https://olog.esss.lu.se">https://olog.esss.lu.se</a></h1><br>

          Comment = Proton Beam Instrumentation<br><h1 style="color:tomato">NOTE: THIS LOGBOOK IS CLOSED FOR NEW ENTRIES. PLEASE USE OLOG INSTEAD: <a href="https://olog.esss.lu.se">https://olog.esss.lu.se</a></h1><br>

	2. Remove options to create new entries, reply, or duplicate.

            List Menu commands = Find, Config, Admin, Logout, Help
            Menu commands = List, Help

2. Backup logbook ELOG production server
	1. `ssh logbook`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `tar cvfz PBI.tar.gz PBI`
	5. `tar cvfz Vac.tar.gz Vac`
	6. `chown juanfestebanmuller:elog PBI.tar.gz Vac.tar.gz`

3. Copy entries to ELOG test server
	1. `ssh ics-elog-test`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `rm -rf PBI Vac`
	5. `scp juanfestebanmuller@logbook.esss.lu.se:/usr/local/elog/logbooks/PBI.tar.gz .`
	6. `scp juanfestebanmuller@logbook.esss.lu.se:/usr/local/elog/logbooks/Vac.tar.gz .`
	7. `tar xvfz PBI.tar.gz`
	8. `tar xvfz Vac.tar.gz`
	9. `systemctl restart elogd`

4. Add all attributes in elog test server, so that when exporting entries they contain all possible metadata:
	
       Attributes = Author, Shift ID, Entry Type, Insert Template, Tags, Subject, Discipline, Subsection, Device Group, Section, Category, Entry Class

5. INFRA stops Olog server and starts the migration service (https://gitlab.esss.lu.se/ics-software/elog-olog-data-migration).

6. Create `Vacuum` tag using the Python notebook `olog_add_vacuum_tag.ipynb`.

7. Import PBI logbook using the Python notebook `olog_mig_PBI.ipynb`.

8. Import Vac logbook using the Python notebook `olog_mig_VAC.ipynb`.

9. Finally INFRA stops the migration service and starts Olog.