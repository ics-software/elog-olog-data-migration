# Fourth migration - 2022 May 24

During this migration, the Cryo and RFS logbooks were migrated from ELOG to Olog. Below it is described the process followed. The Python notebooks contain the output generated when they were run.

## Tests
Tests were carried out on May 18 

## Migration procedure

The migration into the production server took place on May 24, 2022, at around 16:00.

1. First we closed the Cryo logbook in ELOG production server. To do that, the following lines are added to the configuration file:
   1. Add comment about closure: 

		Comment = Cryogenics<br><h1 style="color:tomato">NOTE: THIS LOGBOOK IS CLOSED FOR NEW ENTRIES. PLEASE USE OLOG INSTEAD: <a href="https://olog.esss.lu.se">https://olog.esss.lu.se</a></h1><br>

	2. Remove options to create new entries, reply, or duplicate.

            List Menu commands = Find, Config, Admin, Logout, Help
            Menu commands = List, Help

2. The RFS logbook stays open, but with a note asking to post new entries on Olog:

          Comment = Radio Frequency Systems <br><h1 style="color:tomato">NOTE: THIS LOGBOOK HAS BEEN MIGRATED TO OLOG. IF POSSIBLE, CREATE NEW ENTRIES HERE:<a href="https://olog.esss.lu.se">https://olog.esss.lu.se</a></h1><br>

3. Backup logbook ELOG production server
	1. `ssh logbook`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `tar cvfz RFS.tar.gz RFS`
	5. `tar cvfz Cryo.tar.gz Cryo`
	6. `chown juanfestebanmuller:elog RFS.tar.gz Cryo.tar.gz`

4. Copy entries to ELOG test server
	1. `ssh ics-elog-test`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `rm -rf RFS Cryo`
	5. `scp juanfestebanmuller@logbook.esss.lu.se:/usr/local/elog/logbooks/RFS.tar.gz .`
	6. `scp juanfestebanmuller@logbook.esss.lu.se:/usr/local/elog/logbooks/Cryo.tar.gz .`
	7. `tar xvfz RFS.tar.gz`
	8. `tar xvfz Cryo.tar.gz`
	9. `systemctl restart elogd`

5. Add all attributes in elog test server, so that when exporting entries they contain all possible metadata:
	
       Attributes = Author, Shift ID, Entry Type, Insert Template, Tags, Subject, Discipline, Subsection, Device Group, Section, Category, Entry Class

6. INFRA stops Olog server and starts the migration service (https://gitlab.esss.lu.se/ics-software/ess-olog-product/-/tree/elog-olog-data-migration , https://gitlab.esss.lu.se/ics-software/elog-olog-data-migration).

7. Create new logbooks and tags using the Python notebook `olog_configure_logbooks_and_tags.ipynb`.

8. Import Cryo logbook using the Python notebook `olog_mig_Cryo.ipynb`.

9. Import RFS logbook using the Python notebook `olog_mig_RFS.ipynb`.

10. It was noticed that some links to ELOG entries were not properly migrated on previous ocassions. Run `fix_entries.ipynb` to fix them.

11. Finally INFRA stops the migration service and starts Olog.