# First migration - 2021 Sept. 21

During this migration, the logbooks Operation, On-call, Maintenance, and Studies were migrated from ELOG to Olog. Below it is described the process followed. The Python notebooks contain the output generated when they were run.

## Migration procedure
1. INFRA stops Olog server and starts the migration service (https://gitlab.esss.lu.se/ics-software/elog-olog-data-migration).

2. Then we closed logbooks in ELOG production server. To do that, the following lines are added to the configuration file:
   1. Add comment about closure: 
 
          Comment = Operators shift logbook. <br><h1 style="color:tomato">NOTE: THIS LOGBOOK IS CLOSED FOR NEW ENTRIES. PLEASE USE OLOG INSTEAD: <a href="https://olog.esss.lu.se">https://olog.esss.lu.se</a></h1><br>

	2. Remove options to create new entries, reply, or duplicate.

            List Menu commands = Find, Config, Admin, Logout, Help
            Menu commands = List, Help

3. Add all attributes in elog test server, so that when exporting entries they contain all possible metadata:
	
       Attributes = Author, Shift ID, Entry Type, Insert Template, Tags, Subject, Discipline, Subsection, Device Group, Section, Category, Entry Class

4. Backup logbook ELOG production server
	1. `ssh logbook`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `tar cvfz lb.tar.gz Operation Studies Maintenance On-call`
	5. `chown juanfestebanmuller:elog op.tar.gz`

5. Copy entries to ELOG test server
	1. `ssh ics-elog-test`
	2. `sudo su`
	3. `cd /usr/local/elog/logbooks/`
	4. `rm -rf Operation On-call/ Studies/ Maintenance/`
	5. `scp juanfestebanmuller@logbook.esss.lu.se:/usr/local/elog/logbooks/lb.tar.gz .`
	6. `tar xvfz lb.tar.gz`
	7. `systemctl restart elogd`

6. Create logbooks, tags, and properties in Olog using the notebook `olog_setup_logbooks_tags_and_properties.ipynb`.

7. Import logbooks using the Python notebooks.
    1. First Operations, using the notebook `olog_mig_Operation.ipynb`.
    2. Then the rest: `olog_mig_OnCall.ipynb`, `olog_mig_Maintenance.ipynb`, and `olog_mig_Studies.ipynb`.
    3. By mistake one entry was missing in the Operations logbook, and it was recovered using the script `olog_mig_last_entry_Operations.ipynb`.

8. Finally INFRA stops the migration service and starts Olog.

A bug in the importer cleared the tags in entries belonging to a group or with links to other entries. On April 26, 2022, it was fixed by using the script `fix_tags_first_migration.ipynb`.
