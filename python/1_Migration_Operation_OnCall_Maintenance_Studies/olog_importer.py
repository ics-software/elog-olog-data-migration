from getpass import getpass
from passlib.hash import sha256_crypt
import requests
from requests.auth import HTTPBasicAuth
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from markdownify import markdownify
from datetime import datetime
import magic
import json
import traceback
import re
import uuid
import urllib3
urllib3.disable_warnings()

config_str = '/?cmd=GetConfig'
download_str = '?cmd=download'

# Generate cookie for elog connection (you need to run this at least once)
user = input('Please input the username') or 'juanfestebanmuller'
password = getpass('Please input the password for username '+user+':\n')
auth = HTTPBasicAuth(user, password)
password = sha256_crypt.using(salt='', rounds=5000).hash(password)[4:]

cookie = 'unm=' + user + ';'
cookie += 'upwd=' + password + ';'

request_headers = {'Cookie': cookie}


class OlogImporter(object):

    find_str = '/?mode=Raw&reverse=0'
    rev_logbook_mapping = dict()
    top_logbooks_to_import = ['Machine']

    # Attributes that should be mapped to specific olog attributes
    att_olog_list = ['Author', 'Date', 'Subject', 'Shift ID', 'Entry Type']

    # List of attributes that should be mapped to tags
    att_to_tags_list = ['Entry Class', 'Category', 'Section',
                        'Subsection', 'Device Group', 'Discipline', 'Tags']

    # Attributes that should not be taken into account for the extended metadata property
    att_skip_list = att_olog_list + \
        ['Insert Template', 'Attachment', 'Encoding', 'Reply to', 'In reply to']

    def __init__(self, elog_server, elog_url, olog_server, olog_url):
        self.elog_server = elog_server
        self.olog_server = olog_server
        self.elog_url = elog_url
        self.olog_url = olog_url
        self.session = requests.Session()
        retry = Retry(connect=5, backoff_factor=0.5)
        adapter = HTTPAdapter(max_retries=retry)
        self.session.mount(elog_server, adapter)

    def load_logbooks(self):
        response = self.session.get(
            self.elog_server+config_str, verify=False)
        content = response.content.decode().split('\n')

        #  Get groups:
        for line in content:
            if line.startswith('Top group '):
                top_group = line[len('Top group '):line.find('=')].strip()
                if top_group in self.top_logbooks_to_import:
                    groups = line[line.find('=') + 1:].split(',')
                    self.groups = [group.strip() for group in groups]

        # Get the logbooks
        self.logbooks = []

        #  First get groups:
        for line in content:
            if line.startswith('Group '):
                group = line[len('Group '):line.find('=')].strip()
                if group in self.groups:
                    logbooks_aux = line[line.find('=') + 1:].split(',')
                    logbooks_aux = [lb.strip() for lb in logbooks_aux]
                    self.logbooks.extend(logbooks_aux)

    def set_logbook_mapping(self, logbook_mapping):
        for lb in logbook_mapping:
            for elem in logbook_mapping[lb]:
                self.rev_logbook_mapping[elem] = lb

    def print_logbook_mapping(self):
        print('Logbook mapping:')
        for lb in self.rev_logbook_mapping:
            print(lb + " --> " + self.rev_logbook_mapping[lb])

    # Extract metadata - merging all options available
    def parse_attributes(self):
        response = self.session.get(
            self.elog_server+config_str, verify=False)
        content = response.content.decode().split('\n')

        self.attributes = []
        enable_parsing = False
        for line in content:
            if line.startswith('[global '):
                top_group = line[len('[global '):line.find(']')]
                if top_group in self.top_logbooks_to_import:
                    enable_parsing = True
                else:
                    enable_parsing = False
            elif line.startswith('['):
                logbook = line[1:line.find(']')]
                if logbook in self.logbooks:
                    enable_parsing = True
                else:
                    enable_parsing = False

            # Only consider attributes inside the selected logbook groups
            if enable_parsing and line.startswith('Attributes'):
                attributes_aux = line[line.find('=') + 1:].split(',')
                for att in attributes_aux:
                    att = att.strip()
                    if att not in self.attributes:
                        self.attributes.append(att)

        print('These are the attributes found in the config file:')
        print(self.attributes)

        new_atts = []
        for att in self.attributes:
            if att not in self.att_olog_list and att not in self.att_skip_list and att not in self.att_to_tags_list:
                new_atts.append(att)
        if new_atts != []:
            print(
                'ERROR! The following attributes is on the config file but no rule is defined for it:', new_atts)

    def set_tag_mapping(self, tag_mapping):
        self.tag_mapping = tag_mapping

    def print_tag_mapping(self):
        self.rev_tag_mapping = dict()
        for tm in self.tag_mapping:
            for elem in self.tag_mapping[tm]:
                self.rev_tag_mapping[elem] = tm

        print('Tag mapping:')
        for tm in self.rev_tag_mapping:
            print(tm + " --> " + self.rev_tag_mapping[tm])

    def set_entry_type_mapping(self, entry_type_mapping):
        self.entry_type_mapping = entry_type_mapping

    def print_entry_type_mapping(self):
        self.rev_et_mapping = dict()
        for etm in self.entry_type_mapping:
            for elem in self.entry_type_mapping[etm]:
                self.rev_et_mapping[elem] = etm

        print('Entry type mapping:')
        for etm in self.rev_et_mapping:
            print(etm + " --> " + self.rev_et_mapping[etm])

    def _check_attributes(self, entries):
        # Scan the entries to identify all possible values for attributes that should be mapped to tags in olog and where not in the config file
        att_options = []
        new_atts = []

        atts_dict = {}

        for entry in entries:
            header, msg = entry.split(
                '========================================\n')
            header = header.split('\n')

            for line in header:
                new_att_flag = True
                for att in self.att_to_tags_list:
                    if line.startswith(att):
                        new_att_flag = False
                        att_values = line[len(att+':')+1:].strip()

                        for att_value in att_values.split('|'):
                            att_value = att_value.strip()
                            if att_value != '' and att_value not in att_options:
                                att_options.append(att_value)
                                if atts_dict.get(att) is None:
                                    atts_dict[att] = []
                                atts_dict[att].append(att_value)

                if new_att_flag and line.find(':') != -1:
                    att_name = line[:line.find(':')].strip()
                    if att_name not in self.att_skip_list and att_name not in new_atts:
                        new_atts.append(att_name)

        if new_atts != []:
            print(
                'New attributes were found, please check the attribute lists provided above!')
            print(new_att_flag)
            print('----------------------------------')

        new_tags = []
        for att in att_options:
            if att not in self.rev_tag_mapping.keys():
                new_tags.append(att)

        if new_tags != []:
            print('These options are not mapped to olog tags:')
            print(new_tags)

    def _check_entry_types(self, entries):
        new_ets = []

        for entry in entries:
            header, msg = entry.split(
                '========================================\n')
            header = header.split('\n')

            for line in header:
                if line.startswith('Entry Type:'):
                    et = line[len('Entry Type:')+1:].strip()

                    if et not in self.rev_et_mapping.keys() and et not in new_ets:
                        new_ets.append(et)

        if new_ets != []:
            print(
                'New entry types were found, please check the entry type mapping provided above!')
            print(new_ets)
            raise Exception()

    def _process_html_message(self, msg):
        p_start_table = re.compile('<table')
        p_end_table = re.compile('</table>')
        # Remove reply quotations
        reply_str = '<table align="center" class="replyframe" style="width:98%">'
        close_tab_str = '</table>'
        while (msg.find(reply_str) != -1):
            open_tag_idx = msg.find(reply_str)
            close_tag_idx = msg.find(
                reply_str) + msg[msg.find(reply_str):].find(close_tab_str) + len(close_tab_str)
            next_open_tag = open_tag_idx
            while msg[next_open_tag+len(reply_str):close_tag_idx].find('<table') != -1:
                next_open_tag = next_open_tag + \
                    msg[next_open_tag +
                        len(reply_str):close_tag_idx].find('<table') + len('<table')
                close_tag_idx += msg[close_tag_idx:].find(
                    close_tab_str) + len(close_tab_str)
            msg = msg[:open_tag_idx] + msg[close_tag_idx:]

        reply_str = '<table width="98%" align="center" class="replyframe">'
        while (msg.find(reply_str) != -1):
            open_tag_idx = msg.find(reply_str)
            close_tag_idx = msg.find(
                reply_str) + msg[msg.find(reply_str):].find(close_tab_str) + len(close_tab_str)
            next_open_tag = open_tag_idx
            while msg[next_open_tag+len(reply_str):close_tag_idx].find('<table') != -1:
                next_open_tag = next_open_tag + \
                    msg[next_open_tag +
                        len(reply_str):close_tag_idx].find('<table') + len('<table')
                close_tag_idx += msg[close_tag_idx:].find(
                    close_tab_str) + len(close_tab_str)
            msg = msg[:open_tag_idx] + msg[close_tag_idx:]

        # TODO: reformat tables
        # Remove tbody tags that don't have corresponding thead tags
        for r in p_start_table.finditer(msg):
            table_start_idx = r.start()

            table_end_idx = table_start_idx + \
                p_end_table.search(msg[table_start_idx:]).end()

            if msg[table_start_idx:table_end_idx].find('<tbody>') != -1 and msg[table_start_idx:table_end_idx].find('<thead>') == -1:
                new_msg = msg[:table_start_idx]
                # Replace tbody tags by blank spaces so that the indexes won't move
                new_msg += msg[table_start_idx:table_end_idx].replace(
                    '<tbody>', '        ').replace('</tbody>', '         ')

                new_msg += msg[table_end_idx:]

                msg = new_msg

        return msg

    def import_logbook(self, logbook):
        # Get all entries for a given logbook
        response = self.session.get(
            self.elog_server+'/'+logbook+self.find_str, headers=request_headers, verify=False)

        # Convert from binary to ASCII, fix end line, and generate a list of entries
        entries = response.content.decode(encoding="ISO-8859-15")
        entries = entries.replace('\r\n', '\n')
        entries = entries.split('$@MID@$: ')[1:]
        print("Received {0} entries.".format(len(entries)))

        # Load mapping of usernames and full names
        self.users = dict()
        with open('users.csv', 'r') as users_file:
            for line in users_file:
                name, user = line.split(',')
                self.users[name.strip()] = user.strip()

        self._check_attributes(entries)
        self._check_entry_types(entries)
        self._sanity_checks(entries)

        return entries

    def _sanity_checks(self, entries):
        '''
        Sanity checks of the existing entries
        '''

        # First we check that all fields exists and contain the right information
        for entry in entries:
            header, msg = entry.split(
                '========================================\n')
            header = header.split('\n')

            msg_id = header[0]
            author = ''
            date = ''
            subject = ''
            entry_type = ''
            encoding = ''

            shift_id = ''

            for line in header:
                if line.startswith('Author:'):
                    author = line[len('Author:')+1:].strip()
                    if author == '':
                        print('Msg ' + msg_id + ' has empty author field.')
                    if author not in self.users:
                        print('Msg ' + msg_id + ' has unknown author: ' + author)
                elif line.startswith('Date:'):
                    # Parsing date
                    date_str = line[len('Date:')+1:].strip()
                    date = '{0:1.7f}'.format(datetime.strptime(
                        date_str, "%a, %d %b %Y %H:%M:%S %z").timestamp())
                    if date_str == '':
                        print('Msg ' + msg_id + ' has empty date field.')
                elif line.startswith('Shift ID:'):
                    shift_id = line[len('Shift ID:')+1:].strip()
                    if shift_id == '':
                        print('Msg ' + msg_id + ' has empty shift ID field.')
                elif line.startswith('Subject:'):
                    subject = line[len('Subject:')+1:].strip()
                    if subject == '':
                        print('Msg ' + msg_id + ' has empty subject field.')
                elif line.startswith('Entry Type:'):
                    entry_type = line[len('Entry Type:')+1:].strip()
                    if entry_type == '':
                        print('Msg ' + msg_id + ' has empty entry type field.')
                    if entry_type not in self.rev_et_mapping:
                        print('Message ID: ' + msg_id + '. Entry type ' +
                              entry_type + ' not known.')
                elif line.startswith('Encoding:'):
                    encoding = line[len('Encoding:')+1:].strip()
                    if encoding != 'HTML':
                        print('Message ID: ', msg_id)
                        print('encoding = ', encoding)
                elif line.startswith('Attachment:'):
                    attachments_aux = line[len(
                        'Attachment:')+1:].strip().split(',')
                    if attachments_aux == '':
                        print('Msg ' + attachments_aux +
                              ' has empty attachments field.')

            if author == '':
                print('Message ID: ' + msg_id + '. No author defined.')
            if date == '':
                print('Message ID: ' + msg_id + '. No date defined.')
            if shift_id == '':
                print('Message ID: ' + msg_id + '. No shift ID defined.')
            if subject == '':
                print('Message ID: ' + msg_id + '. No subject defined.')
            if entry_type == '':
                print('Message ID: ' + msg_id + '. No entry type defined.')

    def set_shift_id_map(self, shift_id_map):
        self.shift_id_map = shift_id_map

    def set_shift_info_map(self, shift_info_map):
        self.shift_info_map = shift_info_map

    def parse_shift_info(self, entries):
        '''
        Parse all entries to extract shift ID and shift information (leader, phones, etc).
        It also corrects the shift ID if it was wrong or non-existing.
        '''
        # Map for pairs (elog_id, shift_id)
        self.shift_id_map = dict()
        # Map for pairs (shift_id, shift_info)
        self.shift_info_map = dict()
        # Map for pairs (date, shift_info)
        self.shift_info_map_by_date = dict()

        current_shift_id = ''

        for entry in entries:
            header, message = entry.split(
                '========================================\n')

            header = header.split('\n')

            shift_info = ShiftInfo()

            msg_id = header[0]
            date = ''
            entry_type = ''
            reply_to = ''

            parsed_flag = False

            for line in header:
                if line.startswith('Date:'):
                    date_str = line[len('Date:')+1:].strip()
                    date = '{0:1.7f}'.format(datetime.strptime(
                        date_str, "%a, %d %b %Y %H:%M:%S %z").timestamp())
                elif line.startswith('Shift ID:'):
                    shift_id = line[len('Shift ID:')+1:].strip()
                elif line.startswith('Entry Type:'):
                    entry_type = line[len('Entry Type:')+1:].strip()
                elif line.startswith('In reply to:'):
                    reply_to = line[len('In reply to:')+1:].strip()

            # Get the right shift ID
            date_for_id = datetime.fromtimestamp(
                float(date)).strftime('%Y%m%d')

            if reply_to != '':
                shift_id = self.shift_id_map[reply_to]
                shift_info = self.shift_info_map[shift_id]
            else:
                if entry_type == "Shift Start":
                    parsed_flag = shift_info.parse_info(message)

                    if shift_id[:8] == date_for_id and current_shift_id[:8] == date_for_id:
                        if current_shift_id[-1] == 'A':
                            if datetime.fromtimestamp(float(date)).hour > 12:
                                current_shift_id = date_for_id + 'B'
                            else:
                                print('Possible error on msg ' + msg_id +
                                      '. Second shift start entry happened too early.')
                        elif current_shift_id[-1] == 'B':
                            if datetime.fromtimestamp(float(date)).hour > 21:
                                current_shift_id = date_for_id + 'C'
                            else:
                                print('Possible error on msg ' + msg_id +
                                      '. Third shift start entry happened too early.')
                        elif current_shift_id[-1] == 'C':
                            print('Error on msg ' + msg_id +
                                  ': too many shift start entries for a day!')

                if current_shift_id[:8] != date_for_id or len(current_shift_id) != 9:
                    current_shift_id = date_for_id + 'A'
                elif not (current_shift_id[8] == 'A' or current_shift_id[8] == 'B' or current_shift_id[8] == 'C'):
                    current_shift_id = date_for_id + 'A'

                if shift_id != current_shift_id:
                    print('Error on msg ' + msg_id +
                          ': wrong shift ID. elog = ' + shift_id + '. olog = ' + current_shift_id)

                if len(shift_id) == 9 and current_shift_id[:8] == shift_id[:8] and ((current_shift_id[8] == 'A' and shift_id[8] == 'B') or (current_shift_id[8] == 'B' and shift_id[8] == 'C')):
                    print('Possible error on msg ' + msg_id +
                          '. An entry with the next Shift ID happened before the Shift Start entry.')

                shift_info.shift_id = current_shift_id

                if not parsed_flag:
                    shift_info = self.shift_info_map.get(
                        shift_info.shift_id, shift_info)

                self.shift_info_map_by_date[date] = shift_info

                self.shift_info_map[shift_info.shift_id] = shift_info

            self.shift_id_map[msg_id] = shift_info.shift_id

    def export_to_olog(self, logbook, entries):
        self.link_pattern = re.compile(self.elog_url + '(\w+)/(\d+)')

        # Map with pairs elog ID - olog ID
        self.map_entries = dict()
        # Map with pairs elog ID - level
        level_map = dict()

        entries_w_links = []

        # Map with pairs elog ID - reply to elog ID
        entries_in_threads = dict()

        olog_logbook = self.rev_logbook_mapping[logbook]

        for entry in entries:
            header, message = entry.split(
                '========================================\n')

            msg = self._process_html_message(message)

            # Convert to markdown
            md_msg = markdownify(msg)

            tables_in_html = msg.count('<table')
            tables_in_md = md_msg.count('| --- |\n')

            header = header.split('\n')

            elog_metadata = ElogMetadata()
            elog_metadata.logbook = logbook
            elog_metadata.elog_url = self.elog_url

            shift_info = ShiftInfo()

            msg_id = header[0]

            if tables_in_html != tables_in_md:
                print('Error parsing tables in msg ', msg_id)

            # Remove bold formatting in tables
            if md_msg != md_msg.replace('| **', '| ').replace('**\n\n |', ' |').replace('\n\n |', ' |'):
                print('Possible error parsing tables in msg ', msg_id)
            md_msg = md_msg.replace(
                '| **', '| ').replace('**\n\n |', ' |').replace('\n\n |', ' |')
            if md_msg != md_msg.replace('| **', '| ').replace('** |', ' |').replace('\n\n |', ' |'):
                print('Possible error parsing tables in msg ', msg_id)
            md_msg = md_msg.replace(
                '| **', '| ').replace('** |', ' |').replace('\n\n |', ' |')
            # # Remove italic formatting in tables
            md_msg = md_msg.replace('| *', '| ').replace('*\n\n |', ' |')

            # Find entries with links to other elog entries
            if self.link_pattern.findall(md_msg) != []:
                entries_w_links.append(msg_id)

            elog_metadata.msg_id = msg_id
            date = ''
            subject = ''
            entry_type = ''
            encoding = ''
            reply_to = ''
            tags = []
            attachments = []

            for line in header:
                if line.startswith('Author:'):
                    elog_metadata.author = line[len('Author:')+1:].strip()
                    username = self.users.get(
                        elog_metadata.author, elog_metadata.author)
                    if elog_metadata.author not in self.users:
                        print(line)
                        print(username, elog_metadata.author)
                elif line.startswith('Date:'):
                    # Parsing date
                    date_str = line[len('Date:')+1:].strip()
                    date = '{0:1.7f}'.format(datetime.strptime(
                        date_str, "%a, %d %b %Y %H:%M:%S %z").timestamp())
                elif line.startswith('Shift ID:'):
                    elog_metadata.shift_id = line[len('Shift ID:')+1:].strip()
                elif line.startswith('Subject:'):
                    subject = line[len('Subject:')+1:].strip()
                elif line.startswith('Entry Type:'):
                    entry_type = line[len('Entry Type:')+1:].strip()
                    elog_metadata.entry_type = entry_type
                    level = self.rev_et_mapping.get(entry_type, 'Normal')
                elif line.startswith('Encoding:'):
                    encoding = line[len('Encoding:')+1:].strip()
                    if encoding != 'HTML':
                        print('Message ID: ', msg_id)
                        print('encoding = ', encoding)
                elif line.startswith('In reply to:'):
                    reply_to = line[len('In reply to:')+1:].strip()
                    elog_metadata.reply_to = reply_to
                    entries_in_threads[msg_id] = reply_to
                elif line.startswith('Attachment:'):
                    attachments_aux = line[len(
                        'Attachment:')+1:].strip().split(',')
                    attachments = [att.strip() for att in attachments_aux]
                else:
                    # Map other attributes to tags
                    att_name = line[:line.find(':')].strip()
                    msg_atts = line[line.find(':')+1:].strip()

                    if att_name == 'Section':
                        elog_metadata.section = msg_atts
                    if att_name == 'Subsection':
                        elog_metadata.subsection = msg_atts
                    if att_name == 'Entry Class':
                        elog_metadata.entry_class = msg_atts
                    if att_name == 'Discipline':
                        elog_metadata.discipline = msg_atts
                    if att_name == 'Category':
                        elog_metadata.category = msg_atts
                    if att_name == 'Device Group':
                        elog_metadata.device_group = msg_atts
                    if att_name == 'Tags':
                        elog_metadata.tags = msg_atts

                    msg_atts = msg_atts.split('|')

                    for att in msg_atts:
                        att = att.strip()
                        if att in self.rev_tag_mapping:
                            tags.append({"name": self.rev_tag_mapping[att]})

            shift_info = self.shift_info_map[self.shift_id_map[msg_id]]

            level_map[int(msg_id)] = level

            try:
                response = self.session.put(self.olog_server + 'logs', json={"owner": username, "description": md_msg, "level": level, "title": subject, "logbooks": [
                    {"name": olog_logbook}], "tags": tags, "properties": [shift_info.json(), elog_metadata.json()], "createdDate": str(date)}, auth=auth, verify=False)
                if response.status_code != 200:
                    print(response.status_code)
                    print(response.content)
                else:
                    olog_id = json.loads(response.content)['id']
                    self.map_entries[int(msg_id)] = olog_id
                    if attachments != [] and attachments != ['']:
                        for att in attachments:
                            resp = self.session.get(self.elog_server+'/'+logbook+'/'+att,
                                                    headers=request_headers, verify=False)
                            if resp.status_code == 200:
                                att_file = resp.content

                                mime = magic.from_buffer(att_file, mime=True)
                                att_files = {'filename': (None, att, 'application/json'),
                                             'fileMetadataDescription': (None, mime, 'application/json'),
                                             'file': (att, att_file, 'application/octet-steam')}
                                resp = self.session.post(self.olog_server + 'logs/attachments/'+str(
                                    olog_id), files=att_files, auth=auth, verify=False)
                                if resp.status_code != 200:
                                    print('Attachment ' + att +
                                          ' could not be uploaded.')
                                    print(resp.status_code)
                                    print(resp.content)

                        # Now find the ID of the attachments in the response and use it to fix embedded images.
                        olog_att = json.loads(resp.content)['attachments']

                        embedded_images = False
                        for att in olog_att:
                            if md_msg.find(att['filename']) != -1:
                                idx_before_file = md_msg[:md_msg.find(
                                    att['filename'])].rfind('![')
                                idx_after_file = md_msg.find(att['filename']) + md_msg[md_msg.find(
                                    att['filename']):].find(att['filename']+')') + len(att['filename']+')')
                                md_msg = md_msg[:idx_before_file] + \
                                    '![](attachment/' + att['id'] + ')' + \
                                    md_msg[idx_after_file:]
                                embedded_images = True

                            fn = att['filename']
                            fn = fn[:13] + '/' + fn[14:]

                            fn_aux = fn+'.png?thumb=1'
                            if md_msg.find(fn_aux) != -1:
                                print(
                                    msg_id + ' : found link to attachment thumbnail')
                                idx_before_file = md_msg[:md_msg.find(
                                    fn_aux)].rfind('![')
                                idx_after_file = md_msg.find(fn_aux) + md_msg[md_msg.find(
                                    fn_aux):].find(fn_aux+')') + len(fn_aux+')')
                                md_msg = md_msg[:idx_before_file] + \
                                    '![](attachment/' + att['id'] + ')' + \
                                    md_msg[idx_after_file:]
                                embedded_images = True

                            fn_aux = fn+'.png'
                            if md_msg.find(fn_aux) != -1:
                                print(
                                    msg_id + ' : found link to attachment thumbnail')
                                idx_before_file = md_msg[:md_msg.find(
                                    fn_aux)].rfind('![')
                                idx_after_file = md_msg.find(fn_aux) + md_msg[md_msg.find(
                                    fn_aux):].find(fn_aux+')') + len(fn_aux+')')
                                md_msg = md_msg[:idx_before_file] + \
                                    '![](attachment/' + att['id'] + ')' + \
                                    md_msg[idx_after_file:]
                                embedded_images = True

                            if md_msg.find('[![](attachment') != -1:
                                print(msg_id + ' : removing link to attachment')
                                md_msg = md_msg.replace(
                                    '[![](attachment', '![](attachment')

                        if embedded_images:
                            # Update the entry with the edited text including embedded images
                            response = self.session.post(self.olog_server + 'logs/'+str(olog_id), json={"id": str(olog_id), "description": md_msg, "level": level, "title": subject, "logbooks": [
                                {"name": olog_logbook}], "properties": [shift_info.json(), elog_metadata.json()]}, auth=auth, verify=False)
                            if response.status_code != 200:
                                print(response.status_code)
                                print(response.content)
            except:
                print('There was a problem submitting entry with elog ID: ' +
                      str(msg_id) + '.')
                traceback.print_exc()

        self._replace_links(entries_w_links, logbook)

        # Now analyse the threads to create groups
        self._create_groups(entries_in_threads, level_map)

    def _replace_links(self, entries_w_links, logbook):
        # Replacing links to elog with links to olog
        for msg_id in entries_w_links:
            olog_id = self.map_entries[int(msg_id)]

            r = self.session.get(self.olog_server + 'logs/'+str(olog_id),
                                 auth=auth, verify=False)

            olog_entry = json.loads(r.content)

            message = olog_entry['description']

            links = self.link_pattern.findall(message)

            for link in links:
                if link[0] == logbook:
                    try:
                        message = message.replace(
                            self.elog_url + link[0] + '/' + link[1], self.olog_url + str(self.map_entries[int(link[1])]))
                    except:
                        print('Error in msg ' + str(msg_id) +
                              ': Failed to convert elog link into olog link.')
                else:
                    print('Message ' + str(msg_id) +
                          ' contains a link to another elog logbook.')

            new_entry = {"id": str(olog_id), "description": message, "level": olog_entry['level'], "title": olog_entry[
                'title'], "logbooks": olog_entry['logbooks'], "properties": olog_entry["properties"]}

            response = self.session.post(self.olog_server + 'logs/'+str(olog_id), json=new_entry,
                                         auth=auth, verify=False)
            if response.status_code != 200:
                print(response.status_code)
                print(response.content)

    def _create_groups(self, entries_in_threads, level_map):
        # Parent elog ID - group object
        groups_map = dict()

        for entry, reply_to in entries_in_threads.items():
            # Skip first and last entries of shifts.
            try:
                if level_map[int(entry)] == 'Shift End' or level_map[int(reply_to)] == 'Shift Start' or (level_map[int(entry)] == 'Fault' and level_map[int(reply_to)] != 'Fault'):
                    continue
            except:
                print('Error in entry {0} and reply_to {1}'.format(
                    entry, reply_to))

            group = groups_map.get(int(reply_to), None)

            # If group doesn't exist, create it and add the parent entry
            if group == None:
                group = EntryGroup()
                groups_map[int(reply_to)] = group

                self._add_entry_to_group(int(reply_to), group)

            self._add_entry_to_group(int(entry), group)
            groups_map[int(entry)] = group

    # Method that adds an entry to a group

    def _add_entry_to_group(self, elog_id, group):
        olog_id = self.map_entries[elog_id]

        r = self.session.get(self.olog_server + 'logs/'+str(olog_id),
                             auth=auth, verify=False)

        olog_entry = json.loads(r.content)

        olog_entry["properties"].append(group.json())

        new_entry = {"id": str(olog_id), "description": olog_entry['description'], "level": olog_entry['level'],
                     "title": olog_entry['title'], "logbooks": olog_entry['logbooks'], "properties": olog_entry["properties"]}

        response = self.session.post(self.olog_server + 'logs/'+str(
            olog_id), json=new_entry, auth=auth, verify=False)

        if response.status_code != 200:
            print('Error adding log entry with Olog id ' +
                  str(olog_id) + ' into a group.')
            print(response.content)

# Creates a new entry group with a random identifier
class EntryGroup(object):
    def __init__(self):
        self.id = uuid.uuid4()

    def json(self):
        return {"name": "Log Entry Group", "attributes": [{"name": "id", "value": str(self.id)}]}


class ShiftInfo(object):

    shift_info_header_new_p = re.compile(
        '<h4>[\s]*Shift[\s]*started.[\s]*</h4>')

    shift_leader_new_p = re.compile(
        '<[\w]+>ShiftLeader[\w\s]*: ([^<]*)</[\w]+>[\s]*<br[\s/]*>[\s]+([\w\s\+]*)')

    operator_w_phone_new_p = re.compile(
        '<[\w]+>Operator[\w\s]*: ([^<]*)</[\w]+>[\s]*<br[\s/]*>[\s]+([\w\s\+]*)')

    operator_no_phone_new_p = re.compile(
        '<[\w]+>Operator[\w\s]*: ([^<]*)</[\w]+>[\s]*</p>')

    phone_new_p = re.compile(
        '<small><b>LCR phone number: ([^\<]+)</b></small>')

    shift_info_pattern_old = re.compile(
        '<tr>[\s]*<td>Shift Leader</td>[\s]*<td>([\w\s]*)</td>')

    def __init__(self):
        self.operator_phone = ''
        self.email = ''
        self.operator = ''
        self.shift_leader = ''
        self.shift_lead_phone = ''
        self.shift_id = ''

    def parse_info(self, message):
        message = message.replace('&nbsp;', ' ')
        shift_info = self.shift_info_header_new_p.findall(message)
        if len(shift_info) >= 1:
            sl = self.shift_leader_new_p.findall(message)
            if len(sl) >= 1:
                (shift_leader, shift_lead_phone) = zip(*sl)
                self.shift_leader = ' | '.join(shift_leader)
                self.shift_lead_phone = ' | '.join(shift_lead_phone)
            operator_w_phone = self.operator_w_phone_new_p.findall(message)
            if len(operator_w_phone) >= 1:
                (operator, operator_phone) = zip(*operator_w_phone)
                self.operator = ' | '.join(operator)
                self.operator_phone = ' | '.join(operator_phone)
            else:
                operator_no_phone = self.operator_no_phone_new_p.findall(
                    message)
                if len(operator_no_phone) >= 1:
                    self.operator = ' | '.join(operator_no_phone)
            return True
        else:
            shift_info = self.shift_info_pattern_old.findall(message)
            if len(shift_info) >= 1:
                self.shift_leader = shift_info[0]
                return True

        return False

    def json(self):
        return {"name": "Shift Info",
                "attributes": [{"name": "Operator Phone", "value": self.operator_phone},
                               {"name": "Shift Lead Email", "value": self.email},
                               {"name": "Operator", "value": self.operator},
                               {"name": "Shift Lead", "value": self.shift_leader},
                               {"name": "Shift Lead Phone",
                                   "value": self.shift_lead_phone},
                               {"name": "Shift ID", "value": self.shift_id}]}


class ElogMetadata(object):
    def __init__(self):
        self.author = ''
        self.shift_id = ''
        self.msg_id = ''
        self.section = ''
        self.subsection = ''
        self.entry_type = ''
        self.entry_class = ''
        self.discipline = ''
        self.category = ''
        self.device_group = ''
        self.reply_to = ''
        self.tags = ''
        self.logbook = ''
        self.elog_url = ''

    def json(self):
        return {"name": "ELOG Metadata",
                "attributes": [{"name": "Author", "value": self.author},
                               {"name": "Tags", "value": self.tags},
                               {"name": "Device Group", "value": self.device_group},
                               {"name": "Section", "value": self.section},
                               {"name": "Discipline", "value": self.discipline},
                               {"name": "Category", "value": self.category},
                               {"name": "Subsection", "value": self.subsection},
                               {"name": "Entry Type", "value": self.entry_type},
                               {"name": "Entry Class", "value": self.entry_class},
                               {"name": "Shift ID", "value": self.shift_id},
                               {"name": "Message ID", "value": self.msg_id},
                               {"name": "URL", "value": self.elog_url +
                                   '/'+self.logbook+'/' + self.msg_id},
                               {"name": "Reply To", "value": self.reply_to}]}
