# Use Maven image to execute build.
FROM maven:3.6.3-openjdk-11 AS maven-build
RUN mkdir phoebus-olog
WORKDIR /phoebus-olog
COPY . .
RUN mvn clean install \
    -DskipTests=true \
    -Dmaven.javadoc.skip=true \
    -Dmaven.source.skip=true \
    -Pdeployable-jar

# Use smaller openjdk image for running.
FROM openjdk:11
# apt clean is run automatically in debian-based images.
RUN apt update && apt install -y wait-for-it
# Run commands as user 'olog'
RUN useradd -ms /bin/bash olog
# Use previous maven-build image.
COPY --from=maven-build /phoebus-olog/target /olog-target
RUN rm -f /olog-target/elog-olog-migration-service-*-javadoc.jar && rm -f /olog-target/elog-olog-migration-service-*-sources.jar
RUN chown olog:olog /olog-target
USER olog
WORKDIR /olog-target
EXPOSE 8282
EXPOSE 8383
CMD java -jar elog-olog-migration-service*.jar